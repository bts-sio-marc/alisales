<?php

return [

    'dashboard' => 'Tableau de bord',
    'customers' => 'Clients',
    'customer' => 'Client',
    'name' => 'Nom',
    'address' => 'Adresse',
    'email' => 'Email',
    'website' => 'Site Web',
    'city' => 'Ville',
    'modify' => 'Modifier',
    'postcode' => 'Code Postal',
    'buildingnumber' => 'N°',
    'delete_confirm' => 'Confirmer la suppression de ce client',
    'cancel' => 'Annuler',
    'delete' => 'Supprimer',
    'orders' => 'Commandes',
    'order' => 'Commande',
    'amount' => 'Montant',
    'amountVTA' => 'Montant TVA',
    'dateTime' => 'Date et heure',

];
