@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
<h1 class="m-0 text-dark"><?php echo __('main.order'); ?></h1>
@stop

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-6">
            <form method="POST" action="{{route('order.destroy', ['order'=>$order])}}">
                @csrf
                @method('DELETE')
                <div class="card card-primary card-outline">
                    <div class="card-body">
                        <?php echo __('main.delete_confirm'); ?>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <a href="{{route('order.index')}}" class="btn btn-default float-left">
                            <?php echo __('main.cancel'); ?>
                        </a>
                        <input type="submit" class="btn btn-danger float-right" value="<?php echo __('main.delete'); ?>">
                    </div>
                </div>
        </div>
        </form>
    </div>
</div>
</div>
@stop
