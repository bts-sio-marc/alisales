<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'usersio',
            'email' => 'usersio@test.fr',
            'email_verified_at' => now(),
            'password' =>  Hash::make('pwsio', ['rounds' => 12,]),
            'remember_token' => Str::random(10),
        ]);
        $user->save();
    }
}
