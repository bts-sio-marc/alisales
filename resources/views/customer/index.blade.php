@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
    <h1 class="m-0 text-dark"><?php echo __('main.customers'); ?></h1>
@stop

@section('content')

    <table id="example" class="table table-bordered table-striped datatable dataTable no-footer" style="width:100%">
        <thead>
        <tr>
            <th><?php echo __('main.name'); ?></th>
            <th><?php echo __('main.address'); ?></th>
            <th><?php echo __('main.email'); ?></th>
            <th><?php echo __('main.website'); ?></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <!-- limité à 30 afin que la page ne charge pas tous les éléments -->
        @foreach ($customers as $customer)
            <x-customer.tr :customer=$customer> </x-customer.tr>
        @endforeach

        </tbody>
        <tfoot>
        <tr>
            <th><?php echo __('main.name'); ?></th>
            <th><?php echo __('main.address'); ?></th>
            <th><?php echo __('main.email'); ?></th>
            <th><?php echo __('main.website'); ?></th>
            <th></th>
        </tr>
        </tfoot>
    </table>
@stop
@section('js')
    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/French.json"
                }
            });
        });
    </script>
@stop
