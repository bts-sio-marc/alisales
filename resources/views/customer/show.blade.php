@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
    <h1 class="m-0 text-dark"><?php echo __('main.customers'); ?></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-3">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <h3 class="profile-username text-center">{{$customer->name}}</h3>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b><?php echo __('main.address'); ?></b> <span class="float-right">{{$customer->buildingNumber}}, {{$customer->street}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.postcode'); ?></b> <span class="float-right">{{$customer->postcode}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.city'); ?></b> <span class="float-right">{{$customer->city}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.email'); ?></b> <span class="float-right">{{$customer->email}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.website'); ?></b>
                                <a class="float-right">
                                    <i class="fa fa-external-link-alt"></i>
                                </a>
                            </li>
                        </ul>

                        <a href="{{route('customer.edit', ['customer'=>$customer])}}" class="btn btn-primary btn-block"><b><?php echo __('main.modify'); ?></b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
            <div class="col-9">
                <dic class="card">
                    <div class="card-header">
                        <?php echo __('main.orders'); ?>
                    </div>
                    <div class="card-body">
                        <table id="orders" class="table table-bordered table-striped datatable dataTable no-footer" style="width:100%">
                            <thead>
                            <tr>
                                <th><?php echo __('main.dateTime'); ?></th>
                                <th><?php echo __('main.amount'); ?></th>
                                <th><?php echo __('main.amountVTA'); ?></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            <!-- limité à 30 afin que la page ne charge pas tous les éléments -->
                            @foreach ($customer->orders as $order)
                                <x-order.tr_customer :order=$order> </x-order.tr_customer>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th><?php echo __('main.dateTime'); ?></th>
                                <th><?php echo __('main.amount'); ?></th>
                                <th><?php echo __('main.amountVTA'); ?></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </dic>
            </div>
        </div>
    </div>
@stop
