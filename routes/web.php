<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/login');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');
Route::resource('customer',\App\Http\Controllers\CustomerController::class)->middleware('auth');
Route::get('delete/{customer}', [\App\Http\Controllers\CustomerController::class, 'delete'])->name('customer.delete')->middleware('auth');
Route::resource('order',\App\Http\Controllers\OrderController::class)->middleware('auth');
Route::get('deleteOrder/{order}', [\App\Http\Controllers\OrderController::class, 'delete'])->name('order.delete')->middleware('auth');
