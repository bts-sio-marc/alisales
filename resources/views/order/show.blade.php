@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
    <h1 class="m-0 text-dark"><?php echo __('main.orders'); ?></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <h3 class="profile-username text-center">{{$order->dateTime}}</h3>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b><?php echo __('main.amount'); ?></b> <span class="float-right">{{$order->amount}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.amountVTA'); ?></b> <span class="float-right">{{$order->amountVTA}}</span>
                            </li>
                            <li class="list-group-item">
                                <b><?php echo __('main.customer'); ?></b> <span class="float-right">{{$order->customer->name}}</span>
                            </li>
                        </ul>

                        <a href="{{route('order.edit', ['order'=>$order])}}" class="btn btn-primary btn-block"><b><?php echo __('main.modify'); ?></b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
    </div>
@stop
