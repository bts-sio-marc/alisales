<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CustomerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Customer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'buildingNumber' => $this->faker->buildingNumber(),
            'street' => $this->faker->streetName(),
            'city' => $this->faker->city(),
            'postcode' => $this->faker->postcode(),
            'website' => $this->faker->url(),
            'email' => $this->faker->safeEmail(),
            'user_id' => $this->faker->numberBetween(1, User::count()),
        ];
    }
}
