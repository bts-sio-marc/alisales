@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
    <h1 class="m-0 text-dark"><?php echo __('main.orders'); ?></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <form method="POST" action="{{route('order.update', ['order'=>$order])}}">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="dateTime"><?php echo __('main.dateTime'); ?></label>
                                <input type="datetime-local" id="dateTime"
                                       name="dateTime-time" value="{{$order->dateTime}}"
                                       min="2018-06-07T00:00" max="2018-06-14T00:00"
                                       class="form-control" disabled>
                            </div>
                            <div class="form-group">
                                <label for="amount"><?php echo __('main.amount'); ?></label>
                                <input type="number" step="0.01" class="form-control" id="amount" name="amount" value="{{$order->amount}}">
                            </div>
                            <div class="form-group">
                                <label for="amountVTA"><?php echo __('main.amountVTA'); ?></label>
                                <input type="number" step="0.01" class="form-control" id="amountVTA" name="amountVTA" value="{{$order->amountVTA}}">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <input type="submit" class="btn btn-primary float-right">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop
