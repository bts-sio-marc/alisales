
<tr>
    <td>
        <a href="{{route('customer.show', ['customer'=>$customer])}}">
            {{$customer->name}}
        </a>
    </td>
    <td>
        <x-customer.address :customer=$customer> </x-customer.address>
    </td>
    <td>
        {{$customer->email}}
    </td>
    <td>
        {{$customer->website}}
    </td>
    <td>
        <a class="btn btn-xs btn-default" href="{{route('customer.show', ['customer'=>$customer])}}">
            <i class="fa fa-eye"></i>
        </a>
        <a class="btn btn-xs btn-default" href="{{route('customer.edit', ['customer'=>$customer])}}">
            <i class="fa fa-pen"></i>
        </a>
        <a class="btn btn-xs btn-danger" href="{{route('customer.delete', ['customer'=>$customer])}}">
            <i class="fa fa-trash"></i>
        </a>
    </td>
</tr>
