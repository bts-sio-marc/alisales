@extends('adminlte::page')

@section('title', 'AliSales')
@section('content_header')
    <h1 class="m-0 text-dark"><?php echo __('main.customers'); ?></h1>
@stop

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <form method="POST" action="{{route('customer.update', ['customer'=>$customer])}}">
                        @csrf
                        @method('PUT')
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name"><?php echo __('main.name'); ?></label>
                                <input type="text" class="form-control" id="name" name="name" value="{{$customer->name}}">
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.buildingNumber'); ?></label>
                                <input type="text" class="form-control" id="buildingNumber" name="buildingNumber" value="{{$customer->buildingNumber}}">
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.address'); ?></label>
                                <textarea class="form-control" id="address" name="address">{{$customer->street}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.postcode'); ?></label>
                                <input type="text" class="form-control" id="postalCode" name="postalCode" value="{{$customer->postcode}}">
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.city'); ?></label>
                                <input type="text" class="form-control" id="city" name="city" value="{{$customer->city}}">
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.email'); ?></label>
                                <input type="email" class="form-control" id="email" name="email" value="{{$customer->email}}">
                            </div>
                            <div class="form-group">
                                <label for="name"><?php echo __('main.website'); ?></label>
                                <input type="text" class="form-control" id="website" name="website" value="{{$customer->website}}">
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <input type="submit" class="btn btn-primary float-right">
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@stop
