
<tr>
    <td>
        <a href="{{route('customer.show', ['customer'=>$order->customer])}}">
            {{$order->customer->name}}
        </a>
    </td>
    <td>
        <a href="{{route('order.show', ['order'=>$order])}}">
            {{$order->dateTime}}
        </a>
    </td>
    <td>
        {{$order->amount}}
    </td>
    <td>
        {{$order->amountVTA}}
    </td>
    <td>
        <a class="btn btn-xs btn-default" href="{{route('order.show', ['order'=>$order])}}">
            <i class="fa fa-eye"></i>
        </a>
        <a class="btn btn-xs btn-default" href="{{route('order.edit', ['order'=>$order])}}">
            <i class="fa fa-pen"></i>
        </a>
        <a class="btn btn-xs btn-danger" href="{{route('order.delete', ['order'=>$order])}}">
            <i class="fa fa-trash"></i>
        </a>
    </td>
</tr>
