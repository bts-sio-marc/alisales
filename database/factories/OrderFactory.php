<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $amount = $this->faker->randomFloat(2,9,9999);
        return [
            'dateTime' => $this->faker->dateTime($max = 'now', $timezone = 'Europe/Paris'),
            'amount' => $this->faker->buildingNumber(),
            'amountVTA' => $this->faker->randomFloat(2,0,$amount),
            'customer_id' => $this->faker->numberBetween(1, Customer::count()),
        ];
    }
}
