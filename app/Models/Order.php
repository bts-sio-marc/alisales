<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    /**
     * @var array
     */
    protected $fillable = ['dateTime', 'amount', 'amountVTA'];

    public function customer() {
        return $this->belongsTo('\App\Models\Customer', 'customer_id');
    }

    public function user() {
        return $this->customer->user();
    }
}
