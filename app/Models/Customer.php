<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Order;

class Customer extends Model
{
    use HasFactory;

    /**
     * @var array
    */
    protected $fillable = ['name', 'buildingNumber', 'street', 'city', 'postcode', 'email', 'website', 'user_id'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = true;

    public function user() {
        return $this->belongsTo('\App\Models\User', 'user_id');
    }

    public function orders() {
        return $this->hasMany(Order::Class);
    }

}
