<?php
$url_buildingNumber = str_replace(' ','+',$customer->buildingNumber);
$url_street = str_replace(' ','+',$customer->street);
$url_postcode = str_replace(' ','+',$customer->postcode);
$url_city = str_replace(' ','+',$customer->city);
$maps_url = 'https://www.google.com/maps/search/?api=1&query='.$url_buildingNumber.'+'.$url_street.'+'.$url_postcode.'+'.$url_city;
?>
{{$customer->buildingNumber}}, {{$customer->street}}<br>
{{$customer->postcode}} - {{$customer->city}} <a href="<?= $maps_url ?>" target="_blank"><i class="fas fa-search-location"></i></a>
